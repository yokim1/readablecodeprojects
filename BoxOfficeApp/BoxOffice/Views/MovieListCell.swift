//
//  MovieListCell.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/09.
//

import UIKit


class MovieListCell : UITableViewCell {
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var userRateLabel: UILabel!
    @IBOutlet weak var reservationGradeLabel: UILabel!
    @IBOutlet weak var reservationRateLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
}
