//
//  DetailPageViewController.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/02/25.
//

import UIKit

class DetailPageViewController: UIViewController {
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    @IBOutlet weak var reservationRateLabel: UILabel!
    @IBOutlet weak var userRatingLabel: UILabel!
    @IBOutlet weak var audienceLabel: UILabel!
    
    @IBOutlet weak var synopsisTextField: UITextView!
    
    var movieRequester = MovieRequester()
    var id: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieRequester.requestIDRelatedMovie(id: self.id){ movieDetail in
            DispatchQueue.main.async {
                self.uploadTextOnScreen(movieData: movieDetail)
            }
        }
    }
    
    func uploadTextOnScreen(movieData: MovieDetail) {
            print("Loading....")
        
        let url = URL(string: movieData.image)
        self.downloadImage(from: url!)
        self.titleLabel.text = movieData.title
        self.dateLabel.text = movieData.date
        self.genreLabel.text = movieData.genre
        self.reservationRateLabel.text = "\(movieData.reservation_rate)"
        self.userRatingLabel.text = "\(movieData.user_rating)"
        self.audienceLabel.text = "\(movieData.audience)"
        self.synopsisTextField.text = movieData.synopsis
            
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> (Void)) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        print("Download Image Started")
        getData(from: url) { data, response, error in
            guard let data = data else { return }
            
            DispatchQueue.main.async() { [weak self] in
                self?.movieImage.image = UIImage(data: data)
            }
        }
    }
    
   
}
