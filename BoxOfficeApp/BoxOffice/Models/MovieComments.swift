//
//  MovieComments.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/01.
//

struct MovieCommentResponse: Decodable {
    let movieId: String
    let comments: [MovieComment]
    
    enum CodingKeys: String, CodingKey {
        case movieId = "movie_id"
        case comments
    }
}

struct MovieComment: Codable {
    let id: String
    let rating: Double
    let timestamp: Double
    let writer: String
    let movieId: String
    let contents: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case rating
        case timestamp
        case writer
        case movieId = "movie_id"
        case contents
    }
}
