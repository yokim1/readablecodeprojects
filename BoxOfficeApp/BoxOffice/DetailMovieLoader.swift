//
//  DetailMovieLoader.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/02/25.
//

import UIKit

public class DetailMovieLoader {
    
    var id : String = ""
    var movieDetailData : MovieDetail?
    
    init() {
        
    }
    
//    func load() {
//        //let session = URLSession(configuration:  .default)
//        if let url = URL(string: "http://connect-boxoffice.run.goorm.io/movie?id=\(id)") {
//            print("http://connect-boxoffice.run.goorm.io/movie?id=\(id)")
//            URLSession.shared.dataTask(with: url) { (data, response, error) in
//
//                if let data = data {
//                do {
//                    let dataFromJson = try JSONDecoder().decode(MovieDetail.self, from: data)
//                    self.movieDetailData = dataFromJson
//                }
//                catch let error {
//                    print(error)
//                    }
//                }
//
//            }.resume()
//        }
//
//    }
    
    func request(){
        var urlComponents = URLComponents(string: "http://connect-boxoffice.run.goorm.io/movie?")
        let idQuery = NSURLQueryItem(name: "id", value: id)
        urlComponents?.queryItems?.append(idQuery as URLQueryItem)

        if let url = urlComponents?.url{
        URLSession.shared.dataTask(with: url) { (data, response, error) in

            if let data = data {
            do {
                let dataFromJson = try JSONDecoder().decode(MovieDetail.self, from: data)
                self.movieDetailData = dataFromJson
            }
                catch let error { print(error) }
            }

            }.resume()
        }
    }
}

//struct MovieDetail : Codable {
//    let synopsis: String
//    let user_rating: Double
//    let date: String
//    let title: String
//    let grade: Int
//    let reservation_rate: Double
//    let duration: Int
//    let director: String
//    let image: String
//    let reservation_grade: Int
//    let genre: String
//    let id: String
//    let audience: Int
//    let actor: String
//}
