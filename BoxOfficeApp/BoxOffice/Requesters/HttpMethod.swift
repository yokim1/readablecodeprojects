//
//  HttpMethod.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/17.
//

enum HTTPMethod: String{
    case get
    case post
}
