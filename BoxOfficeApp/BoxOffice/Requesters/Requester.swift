//
//  Requester.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/10.
//

import UIKit

protocol Requester {
    
    associatedtype ResponseType: Decodable
    
}

extension Requester {
    func request(with urlRequest: URLRequest, completionHandler: @escaping (ResponseType?)->Void) {
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            
            handleError(error)
            
            (response as? HTTPURLResponse).map(handleResponse)
            
            handleData(data, completionHandler: completionHandler)
        }.resume()
    }
}

private extension Requester {
    
    func printLocalizedDescription(_ error: Error) {
        print(error.localizedDescription)
    }
    
    func handleError(_ error: Error?) {
        
        error.map(printLocalizedDescription)
        
        if let error = error {
            print(error.localizedDescription)
        }
    }
    
    func handleResponse(_ response: HTTPURLResponse) {
        print("statusCode: \(response.statusCode)")
    }
    
    func handleData(_ data: Data?, completionHandler: @escaping (ResponseType?)->Void) {
        if let data = data {
            do {
                let response = try JSONDecoder().decode(ResponseType.self, from: data)
                completionHandler(response)
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}

