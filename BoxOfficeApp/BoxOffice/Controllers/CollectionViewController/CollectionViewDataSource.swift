//
//  DataSource.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/09.
//


import UIKit

extension CollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let detailMovieViewController = self.storyboard?.instantiateViewController(identifier: "DetailMovieView") as? DetailMovieViewController {
            detailMovieViewController.movieId = movies[indexPath.row].id
            self.navigationController?.pushViewController(detailMovieViewController, animated: true)
        }
    }
}
