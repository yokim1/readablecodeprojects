//
//  TableViewController.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/01/29.
//

import UIKit

protocol TableViewControllerDelegate: class{
    func passReorderedMovies()
}

class TableViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backButtonTitle = "영화목록"
        
        requestMovies()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction private func menuButtonDidTapped(_ sender: Any) {
        let actionSheet = UIAlertController(title: "정렬방식 선택", message: "어떤 순서로 정렬할까요?", preferredStyle: .actionSheet)
        
        MoviesOrderType.allCases.forEach {
            let orderType = $0
            actionSheet.addAction(UIAlertAction(title: orderType.title, style: .default, handler: { [weak self] _ in
                self?.sortMoviesOrder(by: orderType)
                self?.navigationItem.title = orderType.title
            }))
        }
        actionSheet.addAction(UIAlertAction(title: "취소", style: .cancel))
        self.present(actionSheet, animated: true)
    }
    
    var movies = [Movie]()
}

private extension TableViewController {
    func requestMovies() {
        if let url = BoxOfficeURLConfigurationMaker( path: "movies", query: ["":""]).url {
            let urlRequest = URLRequest(url: url)
            
            MovieRequester().request(with: urlRequest) { [weak self] response in
                DispatchQueue.main.async {
                    guard let response = response else { return }
                    self?.movies = response.movies
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func sortMoviesOrder(by type: MoviesOrderType) {
        self.movies.sort { movieA, movieB in
            switch type {
            case .greatestReservationRate:
                return movieA.reservationRate > movieB.reservationRate
                
            case .greatestUserRating:
                return movieA.date > movieB.date
                
            case .latestDate:
                return movieA.userRating > movieB.userRating
            }
        }
        self.tableView.reloadData()
    }
    
    enum MoviesOrderType: CaseIterable {
        case greatestReservationRate
        case latestDate
        case greatestUserRating
        
        var title: String {
            switch self {
            case .greatestReservationRate:
                return "예매율"
            case .greatestUserRating:
                return "큐레이션"
            case .latestDate:
                return "개봉일"
            }
        }
    }
}
