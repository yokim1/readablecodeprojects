//
//  DataSource.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/09.
//

import UIKit

extension TableViewController: UITableViewDataSource {
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
   }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieListCell", for: indexPath) as? MovieListCell else {return UITableViewCell()}
        
        if indexPath.row >= movies.count {
            return UITableViewCell()
        }
        
        cell.titleLabel.text = String(describing: movies[indexPath.row].title)
        cell.userRateLabel.text = String(describing: movies[indexPath.row].userRating)
        cell.reservationGradeLabel.text = String(describing: movies[indexPath.row].reservationGrade)
        cell.reservationRateLabel.text = String(describing: movies[indexPath.row].reservationRate)
        cell.dateLabel.text = String(describing: movies[indexPath.row].date)
        
        if let urlString = movies[indexPath.row].thumb as String?,
           let url = URL(string: urlString)
        {
            URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                if let data = data {
                    DispatchQueue.main.async {
                        cell.movieImage.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        return cell
    }
}
