//
//  Delegate.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/09.
//

import UIKit

extension TableViewController: UITableViewDelegate {
    //what happens when you click the row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailMovieViewController = self.storyboard?.instantiateViewController(identifier: "DetailMovieView") as? DetailMovieViewController {
            detailMovieViewController.movieId = movies[indexPath.row].id
            self.navigationController?.pushViewController(detailMovieViewController, animated: true)
        }
    }
}
