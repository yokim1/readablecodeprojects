//
//  LotteryPurchaser.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/11/16.
//

class LotteryPurchaser{
    var lotteries: [Lottery] = []
    var balance: Int = 0//= 5000
    
    func purchase(with balance : String? ) {
        
        if let changedBalance = Int(balance ?? ""){
            self.balance = changedBalance
        }
        
        for _ in 0..<self.balance/Lottery.price {
            lotteries.append(Lottery())
        }
    }
    
//    func changeBalance(balance: String? ){
//        if let changedBalance = Int(balance ?? ""){
//            self.balance = changedBalance
//        }
//        else{
//           // self.balance = Int(balance)
//        }
//    }
}
