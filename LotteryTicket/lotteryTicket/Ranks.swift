//
//  Ranks.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/11/19.
//

import Foundation

struct Ranks {
    
    let fifthRanks: [LotteryWinningGrade]
    let fourthRanks: [LotteryWinningGrade]
    let thirdRanks: [LotteryWinningGrade]
    let secondRanks: [LotteryWinningGrade]
    let firstRanks: [LotteryWinningGrade]
    
    
    init(of checkedLotteries: [LotteryWinningGrade]){
        fifthRanks = checkedLotteries.filter {$0.prize == 5000}
        fourthRanks = checkedLotteries.filter {$0.prize == 50_000 }
        thirdRanks = checkedLotteries.filter {$0.prize == 1_500_000}
        secondRanks = checkedLotteries.filter {$0.prize == 30_000_000}
        firstRanks = checkedLotteries.filter {$0.prize == 2_000_000_000}
    }
}
