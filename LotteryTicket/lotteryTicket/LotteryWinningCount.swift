//
//  LotteryWinningGrade.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/11/16.
//

import Foundation

struct LotteryWinningCount {
    let count: Int
    let bonusCount: Int
    
    init(count: Int, bonusCount: Int) {
        self.count = count
        self.bonusCount = bonusCount
    }
}
