//
//  LotteryWinningChecker.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/11/16.
//

struct LotteryWinningChecker {
    
    let targetNumbers: [Int] = [1,4,11,19,25,31]
    let targetBonusNumber: Int = 7

    func checkedLotteries(for lotteries: [Lottery]) -> [LotteryWinningGrade] {
        
        var lotteryWinningGrade : [LotteryWinningGrade] = []
        
        lotteries.forEach{
            let concurrentNumbers = Set(targetNumbers).intersection($0.numbers)
            let concurrentBonusNumbers = Set(minimumCapacity: targetBonusNumber).intersection($0.numbers)
            
            lotteryWinningGrade = addLotteryWinningGrades(for: lotteryWinningGrade, of: concurrentNumbers, of: concurrentBonusNumbers)
        }
        
        return lotteryWinningGrade
    }
    
    private func addLotteryWinningGrades(for lotteryWinningGrade: [LotteryWinningGrade], of concurrentNumbers : Set<Int>, of concurrentBonusNumbers: Set<Int>) -> [LotteryWinningGrade] {
        var lotteryWinningGrade = lotteryWinningGrade
        
        if concurrentNumbers.count == 3 {
            lotteryWinningGrade.append(FifthPlace())
        } else if concurrentNumbers.count == 4 {
            lotteryWinningGrade.append(FourthPlace())
        } else if concurrentNumbers.count == 5 {
            lotteryWinningGrade.append(ThirdPlace())
        } else if concurrentNumbers.count == 5 && concurrentBonusNumbers.count == 1 {
            lotteryWinningGrade.append(SecondPlace())
        } else if concurrentNumbers.count == 6 {
            lotteryWinningGrade.append(FirstPlace())
        }
        
        return lotteryWinningGrade
    }
}
