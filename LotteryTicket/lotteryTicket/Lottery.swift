//
//  LOT.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/10/24.
//

//class를 사용한 이유 설명 가능하기
//struct는 value type인데, class는 reference type이기에
//struct는 property에 변경을 주는 것을 권장 하지 않는다.
//그러나 class는 내부 값을 파라미터로 넘겨 받았을때 변경하는 것이 가능하다
//numbers에는 값을 사입하는 것으로 property에 변경을 줘야한다.

import Foundation

struct Lottery {
    var numbers: Set<Int> = []
    let numberOfLotteryNumbers = 6
    
    //while 문을 써서 중복을 없애봐라
    //  while과 set을 사용해서 중복을 없애봐라
    init() {
        while numbers.count != numberOfLotteryNumbers {
            numbers.insert(Int.random(in: 1...46))
        }
    }
    
    static var price: Int { 1000 }
}
