////
////  PrintResult.swift
////  lotteryTicket
////
////  Created by 김윤석 on 2020/10/25.
////
//
//// struct로 한 이유
//// 내부의 값을 다른 func에서 대입하거나 할 일이 없기 때문에 사용
//// reference type을 사용해서, 쓸데없이 비용을 늘리지 않는다.
//struct Printer {
//
//    //FIXME: printer가 초기화 될때, lotteries를 받고 자체 내부적으로 초기화가 된다.
//    // 그럼으로써 함수들에게  lotteries를 넘겨 줄 필요가 없음을 만들어라 53:50
//    //로또의 기본 정보를 보여준다.
//    func printBasicInformation(_ lotteries: [Lottery?]) {
//        print("프로그램 실행 결과")
//        print("로또 구매 금액은 \(balance)원 입니다.")             // balance가 나중에 바뀔수도 있기 때문에, 변수를 사용했다.
//        print("지난주 당첨 번호는 \(targetNumbers)입니다")         // 당첨 번호가 바뀔수 있기 때문에, 변수로 사용
//        print("보너스 볼은 \(targetBonusNumbers)입니다")          // 보너스 번호가 바뀔수 있기 때문에, 변수로 사용
//
//        if let price = lotteries[0]?.price {
//            print("\(balance/price)개를 구매했습니다.") // balance나 price가 변할것을 대비해서 변수로 사용
//        }
//        for index in 0..<lotteries.count {
//            if let lottery = lotteries[index] {
//                print("\(lottery.numbers)")
//            }       // 뽑은 5개의 값을 보여준다.
//        }
//    }
//
//    //로또의 결과값을 보여준다.
//    func printResult(_ concurrentCount: [Int], _ rateOfReturn: Int) {
//        print("당첨 통계는 아래와 같습니다.")
//        print("3개 일치(5000원): \(concurrentCount[0])")                      // 3개 숫자가 일치
//        print("4개 일치(50,000원): \(concurrentCount[1])")                    // 4개 숫자가 일치
//        print("5개 일치(1,500,000원): \(concurrentCount[2])")                 // 5개 숫자가 일치
//        print("5개 일치, 보너스 볼 일치(30,000,000원): \(concurrentCount[3])")    // 5개 숫자와 1개 보너스가 일치
//        print("6개 일치(2,000,000,000): \(concurrentCount[4])")               // 6개 숫자가 일치
//        print("총 수익률은 \(rateOfReturn)입니다.")                              // 총 수익률 표시
//    }
//
//    func printEverything() {
//        let calculator = Calculator()
//
//        //FIXME: printer의 역할이 아닌데, lottery를 initialize하고 있다.
//        // print만하는 느낌이 아닌게 느껴진다.
//        let lotteries:[Lottery?] = [Lottery(), Lottery(), Lottery(), Lottery(), Lottery()]
//
////        lotteries[0]?.numbers = [8,21,23,41,42,43]
////        lotteries[1]?.numbers = [3,5,11,16,32,38]
////        lotteries[2]?.numbers = [7,11,16,35,36,44]
////        lotteries[3]?.numbers = [1,8,11,31,41,42]
////        lotteries[4]?.numbers = [13,14,16,38,42,45]
//
//        printBasicInformation(lotteries)
//        //mutating을 풀면 아래 2개가 문제
//        let concurrentCount = calculator.calculateMatchingNumberCounts(lotteries)
//        if let rateOfReturn = calculator.calculateRateOfReturn(concurrentCount) {
//            printResult(concurrentCount, rateOfReturn)
//        }
//    }
//}
