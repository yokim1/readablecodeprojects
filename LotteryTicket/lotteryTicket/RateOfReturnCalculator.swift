////
////  Calculate.swift
////  lotteryTicket
////
////  Created by 김윤석 on 2020/10/25.
////
//
////
//class concurrCount{
//    
//}
//
struct RateOfReturnCalculator {
    
    func rateOfReturn(of checkedLotteries: [LotteryWinningGrade], for purchaser: LotteryPurchaser) -> Int {
        
        let totalprize = checkedLotteries.reduce(0){ $0 + $1.prize }
        let rateOfReturn = (totalprize / purchaser.balance) * 100
        
        return rateOfReturn
    }
}

//struct Calculator {
//
//    //수익률을 계산해서 되돌려준다.
//    // 이 func의 return이 반드시 OPTIONAL이어야하는가??? yes
//    func calculateRateOfReturn(_ concurrentCount: [Int]) -> Int? {
//        //FIXME: nil이 없는데 optional을 return한다.
//        //FIXME: list라고 쓰지 않고, Prizes라고 쓴다.
//        //FIXME: sideeffect가 없다면, 동사로 쓰면 안된다.
//        // remove first를 사용하지 마라
//        // foreach, map, compact map, flatmap, reduce을 통해서 만드는 것이 더 안전하다.
//        //prize를 return해주는 애가 있으면, 좋겠다는 생각을 한다. 39:21
//        
//        var prizeList: [Int] = [5000, 5_0000, 150_0000, 300_00000, 2_0000_0000] // 관리를 편하게 하기 위해 배열로 상금들을 정리
//        //concurrentCount가 마음에 안든다.
//        //인덱스 0번에 있는것이 5000원과 관련있다는 것을 나밖에 모를 것이다.
//        //매직 넘버가 너무 많다.
//        //너무 순서에 맻혀져 있다.
//        var concurrentCount: [Int] = concurrentCount
//        var maxPrize = 0
//        
//        //FIXME: for each를 사용해서 고쳐보자. 28:37, logic도 틀렸다.
//        while 0 < prizeList.count {
//            
//            if let concurrentCountFirst = concurrentCount.first,
//               let prizeListFirst = prizeList.first {
//                
//                if maxPrize < concurrentCountFirst * prizeListFirst {
//                    maxPrize = concurrentCountFirst * prizeListFirst
//                } else {
//                    concurrentCount.removeFirst()
//                    prizeList.removeFirst()
//                }
//            }
//        }
//        
//        //prizeList.reduce(maxPrize, <#T##nextPartialResult: (Result, Int) throws -> Result##(Result, Int) throws -> Result#>)
//        
//        return (maxPrize/balance) * 100     // maxPrize와 내가 투자했던 balance를 이용해서 수익률을 return한다.
//    }
//    
//    //당첨번호와 로또번호들과의 일치를 계산한다.
//   func calculateMatchingNumberCounts( _ lotteries: [Lottery?]) -> [Int] { // calculate concurrentCount
//        var count: Int
//        var bonusCount: Int
//        var concurrentCount: [Int] = [0,0,0,0,0]
//    
//        for lotteryIndex in 0..<lotteries.count { // 로또의 갯수만큼 for loop를 돌린다.
//            (count, bonusCount) = calculateCountsAndBonusCounts(lotteries, lotteryIndex)
//            concurrentCount = countConccurentCounts( concurrentCount, count, bonusCount)
//        }
//        return concurrentCount
//    }
//    
//    // Count and BonusCount
//    private func calculateCountsAndBonusCounts(_ lotteries: [Lottery?], _ lotteryIndex: Int) -> (Int, Int) {
//        var count: Set<Int> = []
//        if let lotteryNumber = lotteries[lotteryIndex] {
//            count = Set(targetNumbers).intersection(Set(lotteryNumber.numbers))
//        }
//        let bonus: Set<Int> = Set(targetNumbers).intersection(Set(minimumCapacity: targetBonusNumbers))
//        return (count.count, bonus.count)
//    }
//    
//    //FIXME: 좀더 제대로 고쳐라 1:21:10
//    //카운트가 특정 숫자라면, 일치값 카운트를 늘린다.
//    private func countConccurentCounts( _ concurrentCount: [Int], _ count: Int, _ bonusCount: Int) -> [Int] {
//        var concurrentCount: [Int] = concurrentCount
//        var goAround = 0
//        
//        if (count == 3) {
//            concurrentCount.append(concurrentCount.removeFirst() + 1)
//            goAround = 4
//        }
//        else if (count == 4) {
//            concurrentCount.append(concurrentCount.removeFirst() + 1)
//            goAround = 3
//        }
//        else if (count == 5) {
//            concurrentCount.append(concurrentCount.removeFirst() + 1)
//            goAround = 2
//        }
//        else if (count == 5 && bonusCount <= 1) {
//            concurrentCount.append(concurrentCount.removeFirst() + 1)
//            goAround = 11
//        }
//        else if (count == 6) {
//            concurrentCount.append(concurrentCount.removeFirst() + 1)
//            goAround = 0
//        }
//        
//        for _ in 0..<goAround {
//            concurrentCount.append(concurrentCount.removeFirst())
//        }
//        
//        return concurrentCount
//    }
//}
