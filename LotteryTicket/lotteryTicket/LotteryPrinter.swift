//
//  LotteryPrinter.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/11/16.
//

struct LotteryPrinter {

    func printResult(of checkedLotteries: [LotteryWinningGrade], for purchaser: LotteryPurchaser) -> String {
        
        //이것은 다른것으로 어떻게 해결 못하는가?
        let lowWinningChecker = LotteryWinningChecker()
        
        //print 해야하는것
        print("프로그램 실행 결과")
        print("로또 구매 금액은 \(purchaser.balance)원 입니다.")             // balance가 나중에 바뀔수도 있기 때문에, 변수를 사용했다.
        print("지난주 당첨 번호는 \(lowWinningChecker.targetNumbers)입니다")         // 당첨 번호가 바뀔수 있기 때문에, 변수로 사용
        print("보너스 볼은 \(lowWinningChecker.targetBonusNumber)입니다")          // 보너스 번호가 바뀔수 있기 때문에, 변수로 사용
        
//      뽑은 5개의 값들을 전부 보여준다.
        purchaser.lotteries.forEach {print($0.numbers)}
        
        let fifthRanks = checkedLotteries.filter {$0.prize == 5000}
        let fourthRanks = checkedLotteries.filter {$0.prize == 50_000 }
        let thirdRanks = checkedLotteries.filter {$0.prize == 1_500_000}
        let secondRanks = checkedLotteries.filter {$0.prize == 30_000_000}
        let firstRanks = checkedLotteries.filter {$0.prize == 2_000_000_000}
        
//      몇개가 일치하는지 보여준다.
        print("당첨 통계는 아래와 같습니다.")
        print("3개 일치(5000원): \(fifthRanks.count)")                      // 3개 숫자가 일치
        print("4개 일치(50,000원): \(fourthRanks.count)")                    // 4개 숫자가 일치
        print("5개 일치(1,500,000원): \(thirdRanks.count)")                 // 5개 숫자가 일치
        print("5개 일치, 보너스 볼 일치(30,000,000원): \(secondRanks.count)")    // 5개 숫자와 1개 보너스가 일치
        print("6개 일치(2,000,000,000): \(firstRanks.count)")               // 6개 숫자가 일치
    
        let calculator = RateOfReturnCalculator()
        
        print("총 수익률은 \(calculator.rateOfReturn(of: checkedLotteries, for: purchaser))입니다.")// 총 수익률 표시
        
        return """
            프로그램 실행 결과
            로또 구매 금액은 \(purchaser.balance)원 입니다. 나중에 바뀔수도 있기 때문에, 변수를 사용했다.
            지난주 당첨 번호는 \(lowWinningChecker.targetNumbers)입니다.
            보너스 볼은 \(lowWinningChecker.targetBonusNumber)입니다.
            당첨 통계는 아래와 같습니다.
            3개 일치(5000원): \(fifthRanks.count)
            4개 일치(50,000원): \(fourthRanks.count)
            5개 일치(1,500,000원): \(thirdRanks.count)
            5개 일치, 보너스 볼 일치(30,000,000원): \(secondRanks.count)
            6개 일치(2,000,000,000): \(firstRanks.count)
            총 수익률은 \(calculator.rateOfReturn(of: checkedLotteries, for: purchaser))입니다.
            """
    }
}
