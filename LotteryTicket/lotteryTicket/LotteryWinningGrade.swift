//
//  LotteryWinningGrade.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/11/19.
//

import Foundation
protocol LotteryWinningGrade {
    var prize: Int { get };
}

struct FirstPlace: LotteryWinningGrade {
    let prize: Int = 2_000_000_000
}

struct SecondPlace: LotteryWinningGrade {
    let prize: Int = 30_000_000
}

struct ThirdPlace: LotteryWinningGrade {
    let prize: Int = 1_500_000
}

struct FourthPlace: LotteryWinningGrade {
    let prize: Int = 50_000
}

struct FifthPlace: LotteryWinningGrade {
    let prize: Int = 5000
}


