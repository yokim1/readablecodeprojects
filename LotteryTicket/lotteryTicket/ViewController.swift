//
//  ViewController.swift
//  lotteryTicket
//
//  Created by 김윤석 on 2020/10/24.
//

//fillRandomValue에서 중복되는 값 없애는 로직 추가
//convention에 잘따랐는가 함수명, 변수명등을 검사하기
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var balanceInputField: UITextField!
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var labelField: UILabel!
    
    let purchaser = LotteryPurchaser()
    let winningChecker = LotteryWinningChecker()
    let printer = LotteryPrinter()
    
    var resultText: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func purchaseButtonDidTap(_ sender: Any) {
        let balance = balanceInputField.text
        
        self.purchaser.purchase(with: balance)
        let lotteries: [Lottery] = self.purchaser.lotteries
        var checkedLotteries: [LotteryWinningGrade] = self.winningChecker.checkedLotteries(for: lotteries)
        labelField.text = self.printer.printResult(of: checkedLotteries, for: purchaser)
    }
    
}
