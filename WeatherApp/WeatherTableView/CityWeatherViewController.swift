//
//  CityWeatherViewController.swift
//  WeatherTableView
//
//  Created by 김윤석 on 2021/01/21.
//

import UIKit

protocol CityWeatherViewControllerDelegate : class {
    func HomeButtonWasTapped(_ cityWeatherViewController: CityWeatherViewController)
}

class CityWeatherViewController: UIViewController {

    @IBOutlet weak var upperTab: UINavigationItem!
    
    @IBOutlet weak var weather: UILabel!
    @IBOutlet weak var celsius: UILabel!
    @IBOutlet weak var rainfall_probability: UILabel!
    
    var cityData : CityData?
    
    var delegate : CityWeatherViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let celsius = cityData?.celsius,
           let rainfall_probability = cityData?.rainfall_probability {
            self.celsius.text = "섭씨: \(String(celsius))"
            self.rainfall_probability.text = "강수확률: \(String(rainfall_probability))"
        }
    }
    
    @IBAction func HomeButtonWasTapped(_ sender: Any) {
        delegate?.HomeButtonWasTapped(self)
    }
}
