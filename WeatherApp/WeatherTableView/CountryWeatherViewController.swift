//
//  CountryWeatherViewController.swift
//  WeatherTableView
//
//  Created by 김윤석 on 2021/01/21.
//

import UIKit

protocol CountryWeatherViewControllerDelegate {
    func HomeButtonWasTapped(_ countryWeatherViewController: CountryWeatherViewController)
}

class CountryWeatherViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CityWeatherViewControllerDelegate {

    var data : [CityData]?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var upperTab: UINavigationItem!

    var delegate : CountryWeatherViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //the number of the cells
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return data?.count ?? 0
    }
    //
    //how you going to represent the each cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CityDataListCell else {

            return UITableViewCell()
        }
        cell.textLabel?.text = data?[indexPath.row].city_name
        cell.update(String((data?[indexPath.row].rainfall_probability)!))
        
        return cell
    }
    
    //if this cell is clicked
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cityWeatherViewController = self.storyboard?.instantiateViewController(identifier: "CityWeather") as? CityWeatherViewController {
            cityWeatherViewController.cityData = data?[indexPath.row]
            cityWeatherViewController.upperTab.title = data?[indexPath.row].city_name
            cityWeatherViewController.delegate = self
            
            self.navigationController?.pushViewController(cityWeatherViewController, animated: true)
        }
    }
    
    func HomeButtonWasTapped(_ cityWeatherViewController: CityWeatherViewController) {
        cityWeatherViewController.navigationController?.popViewController(animated: true)
        self.delegate?.HomeButtonWasTapped(self)
    }
}

class CityDataListCell : UITableViewCell {
    @IBOutlet weak var rainfall_probability: UILabel!
    
    func update(_ str : String){
        rainfall_probability.text = "강수량: \(str)"
    }
}
