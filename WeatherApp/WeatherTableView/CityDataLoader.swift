//
//  DataLoader2.swift
//  WeatherTableView
//
//  Created by 김윤석 on 2021/01/22.
//

import Foundation
public class CityDataLoader {
    @Published var cityData = [CityData]()
    
    init(_ addr: String){
        load(addr)
    }
    
    func load(_ addr : String){
        print(addr)
        if let fileLocation = Bundle.main.url(forResource: addr, withExtension: "json"){
            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([CityData].self, from: data)
                self.cityData = dataFromJson
            } catch {
                
                print(error)
            
            }
        }
    }
}

struct CityData: Codable {
    
    var city_name : String
    var state : Int
    var celsius : Float
    var rainfall_probability : Int
}
