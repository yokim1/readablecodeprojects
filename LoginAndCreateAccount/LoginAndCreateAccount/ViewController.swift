//
//  ViewController.swift
//  LoginAndCreateAccount
//
//  Created by 김윤석 on 2021/01/13.
//

import UIKit

class ViewController: UIViewController, SignUpviewControllerDelegate {

    @IBOutlet weak var IDLabel: UITextField!
    @IBOutlet weak var PasswordLabel: UITextField!
    
    let userInformation = UserInformation.self
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapSignInButton(_ sender: Any) {
        
        if let userInputID = IDLabel.text, let userInputPassword = PasswordLabel.text{
            if(isUserInputIDandPasswordExist(userInputID, userInputPassword)){
                print("success")
                return
            }
        } else{
            print("nil")
            return
        }
        print("fail")
    }
    
    @IBAction func didTapSignUpButton(_ sender: Any) {
        if let signUpViewController = self.storyboard?.instantiateViewController(identifier: "SignUpViewController") as? SignUpViewController {
            signUpViewController.modalTransitionStyle = .coverVertical
            signUpViewController.modalPresentationStyle = .fullScreen
            signUpViewController.delegate = self
            
            self.present(signUpViewController, animated: true)
        }
    }
    
    //user ID and Password exist?
    private func isUserInputIDandPasswordExist(_ userInputID: String, _ userInputPassword: String) -> Bool {
        
        if (userInformation .shared.ID == userInputID && userInformation .shared.password == userInputPassword){
            return true;
        }
        return false
    }
    
    func didTapCancelButton(_ SingUpViewController: SignUpViewController) {
        SingUpViewController.dismiss(animated: true, completion: nil)
    }
}
